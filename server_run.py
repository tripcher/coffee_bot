import uvicorn

from app.server import app as app_server


if __name__ == '__main__':
    uvicorn.run(app_server, host="0.0.0.0", port=8005)
