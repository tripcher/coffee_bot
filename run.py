from termcolor import cprint

from app import bot
import app.handlers

if __name__ == '__main__':
    cprint("Start polling bot \"{}\"".format(bot.get_me().first_name), 'red')

    print("Handlers:")
    for handler in bot.message_handlers:
        cprint(handler.get('function').__name__, 'yellow')

    print("Callback query handlers:")
    for handler in bot.callback_query_handlers:
        cprint(handler.get('function').__name__, 'yellow')

    bot.polling()

    cprint("Stop bot...", 'red')
