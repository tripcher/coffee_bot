import typing
import collections
import requests

from app.main import api, utils
from app.core import cache
from app.main.domain import models, events, storage
from app.main.domain.orders import models as orders_models, schemas as orders_schemas


class MixinServerStorage(object):
    @staticmethod
    def _get_order_server(user_id) -> typing.Optional[orders_models.Order]:
        """
        Возвращает текущий заказ пользователя c сервера:
        Args:
            user_id:

        Returns:
            typing.Optional[orders_models.Order]
        """
        try:
            order_data: typing.Dict[str, str] = api.get_active_order(user_id)
        except requests.exceptions.HTTPError as exc:
            response: requests.Response = exc.response
            if response.status_code == 404:
                return None
            else:
                raise
        else:
            return orders_models.Order(**order_data)

    @staticmethod
    def _create_order_server(user_id: str, order: orders_models.Order) -> orders_models.Order:
        """
        Создает текущий заказ на сервере.
        Args:
            user_id (str):
            order (orders_models.Order):
        Returns:
            orders_models.Order:
        """
        data = orders_schemas.OrderBase(**order.dict(), user_id=user_id).dict()

        order_data = api.create_order(data=data)

        return orders_models.Order(**order_data)

    @staticmethod
    def _update_order_server(user_id: str, order: orders_models.Order) -> orders_models.Order:
        """
        Обновляет текущий заказ на сервере.
        Args:
            user_id (str):
            order (orders_models.Order):
        Returns:
            orders_models.Order:
        """
        data = orders_schemas.OrderBase(**order.dict(exclude={'user_id'}), user_id=user_id).dict()

        order_data = api.update_order(data=data)

        return orders_models.Order(**order_data)


class MemoryStorage(MixinServerStorage, storage.Storage):

    dialogs_history: typing.DefaultDict[str, typing.List[str]] = collections.defaultdict(list)
    orders: typing.Dict[str, orders_models.Order] = dict()

    def new_dialog_history(self, user_id: str) -> None:
        self.dialogs_history[user_id] = [utils.callback_data(event=events.CALLBACK_START)]

    def get_dialog_history(self, user_id: str) -> typing.List[str]:
        return self.dialogs_history[user_id]

    def add_dialog_history(self, user_id: str, element: str) -> None:
        self.dialogs_history[user_id].append(element)

    def get_last_element_dialog_history(self, user_id: str) -> str:
        if not len(self.dialogs_history[user_id]):
            return utils.callback_data(event=events.CALLBACK_START)

        return self.dialogs_history[user_id][-1]

    def pop_last_element_dialog_history(self, user_id: str) -> str:
        return self.dialogs_history[user_id].pop()

    def add_order(self, user_id: str) -> orders_models.Order:
        try:
            order = self.get_order(user_id)
        except KeyError:
            order = self._create_order_server(user_id, orders_models.Order())
            self.orders[user_id] = order

        return order

    def edit_order(self, user_id: str, order: orders_models.Order) -> orders_models.Order:
        self.orders[user_id] = order
        return order

    def get_order(self, user_id: str) -> orders_models.Order:
        order = self.orders.get(user_id)

        if order is None:
            order = self._get_order_server(user_id)

            if order is None:
                raise KeyError()

            self.orders[user_id] = order

        return order

    def add_product_for_order(self, user_id: str, product: models.Product) -> None:
        order = self.get_order(user_id)
        order.add_product(product)
        order = self._update_order_server(user_id, order)
        self.edit_order(user_id, order)

    def add_additive_product_for_order(self, user_id: str, additive: models.Product, product_id: int) -> None:
        order = self.get_order(user_id)
        additives = order.additives.get(product_id, [])
        additives.append(additive)
        order.additives[product_id] = additives
        order = self._update_order_server(user_id, order)
        self.edit_order(user_id, order)

    def del_product_for_order(self, user_id: str, product: models.Product, full=False) -> None:
        order = self.get_order(user_id)
        if full:
            order.products = [item for item in order.products if item.id != product.id]
        else:
            order.products.remove(product)

        order = self._update_order_server(user_id, order)
        self.edit_order(user_id, order)

    def del_additive_product_for_order(
            self,
            user_id: str,
            additive: models.Product,
            product_id: int,
            full=False
    ) -> None:
        order = self.get_order(user_id)

        if full:
            order.additives[product_id] = [item for item in order.additives[product_id] if item.id != additive.id]
        else:
            order.additives[product_id].remove(additive)

        order = self._update_order_server(user_id, order)
        self.edit_order(user_id, order)

    def get_last_product_id_for_order(self, user_id: str) -> typing.Optional[int]:
        order = self.get_order(user_id)
        return order.products[-1].id if len(order.products) > 0 else None

    def get_last_product_id_for_dialog_history(self, user_id: str) -> typing.Optional[int]:
        history_product = [item for item in self.dialogs_history[user_id] if item.startswith(events.CALLBACK_PRODUCT)]

        if len(history_product) == 0:
            return None

        event, _, product_id = history_product[-1].rpartition('--')

        return int(product_id) if product_id is not None else None


class MemcachedStorage(MixinServerStorage, storage.Storage):

    client = cache.get_memcache()

    prefix_dialog_history = 'pdh_'
    prefix_order = 'po_'

    def _dialog_history_key(self, value: str) -> str:
        return self.prefix_dialog_history + value

    def _order_key(self, value: str) -> str:
        return self.prefix_order + value

    def new_dialog_history(self, user_id: str) -> None:
        self.client.set(self._dialog_history_key(user_id), [utils.callback_data(event=events.CALLBACK_START)])

    def get_dialog_history(self, user_id: str) -> typing.List[str]:
        return self.client.get(self._dialog_history_key(user_id), default=[])

    def add_dialog_history(self, user_id: str, element: str) -> None:
        dialog_history = self.client.get(self._dialog_history_key(user_id), default=[])
        dialog_history.append(element)

        self.client.set(self._dialog_history_key(user_id), dialog_history)

    def get_last_element_dialog_history(self, user_id: str) -> str:
        dialog_history = self.client.get(
            self._dialog_history_key(user_id),
            default=[utils.callback_data(event=events.CALLBACK_START)]
        )

        return dialog_history[-1]

    def pop_last_element_dialog_history(self, user_id: str) -> str:
        dialog_history = self.client.get(self._dialog_history_key(user_id), default=[])
        result = dialog_history.pop()
        self.client.set(self._dialog_history_key(user_id), dialog_history)
        return result

    def add_order(self, user_id: str) -> orders_models.Order:
        try:
            order = self.get_order(user_id)
        except KeyError:
            order = self._create_order_server(user_id, orders_models.Order())
            self.client.set(self._order_key(user_id), order.dict())

        return order

    def edit_order(self, user_id: str, order: orders_models.Order) -> orders_models.Order:
        self.client.set(self._order_key(user_id), order.dict())
        return order

    def get_order(self, user_id: str) -> orders_models.Order:
        order_data = self.client.get(self._order_key(user_id))

        if order_data is None:
            order = self._get_order_server(user_id)

            if order is None:
                raise KeyError()
            self.client.set(self._order_key(user_id), order.dict())
        else:
            order = orders_models.Order(**order_data)

        return order

    def add_product_for_order(self, user_id: str, product: models.Product) -> None:
        order = self.get_order(user_id)
        order.add_product(product)
        order = self._update_order_server(user_id, order)
        self.edit_order(user_id, order)

    def add_additive_product_for_order(self, user_id: str, additive: models.Product, product_id: int) -> None:
        order = self.get_order(user_id)
        additives = order.additives.get(product_id, [])
        additives.append(additive)
        order.additives[product_id] = additives
        order = self._update_order_server(user_id, order)
        self.edit_order(user_id, order)

    def del_product_for_order(self, user_id: str, product: models.Product, full=False) -> None:
        order = self.get_order(user_id)
        if full:
            order.products = [item for item in order.products if item.id != product.id]
        else:
            order.products.remove(product)

        order = self._update_order_server(user_id, order)
        self.edit_order(user_id, order)

    def del_additive_product_for_order(
            self,
            user_id: str,
            additive: models.Product,
            product_id: int,
            full=False
    ) -> None:
        order = self.get_order(user_id)

        if full:
            order.additives[product_id] = [item for item in order.additives[product_id] if item.id != additive.id]
        else:
            order.additives[product_id].remove(additive)

        order = self._update_order_server(user_id, order)
        self.edit_order(user_id, order)

    def get_last_product_id_for_order(self, user_id: str) -> typing.Optional[int]:
        order = self.get_order(user_id)
        return order.products[-1].id if len(order.products) > 0 else None

    def get_last_product_id_for_dialog_history(self, user_id: str) -> typing.Optional[int]:
        dialog_history = self.client.get(self._dialog_history_key(user_id), default=[])

        history_product = [item for item in dialog_history if item.startswith(events.CALLBACK_PRODUCT)]

        if len(history_product) == 0:
            return None

        event, _, product_id = history_product[-1].rpartition('--')

        return int(product_id) if product_id is not None else None
