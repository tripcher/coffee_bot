import typing
import os
import json

import app
from app.main.domain import models
from app.main.domain import repositories
from app.main import api, exceptions


class FileProductRepository(repositories.ProductRepository):

    """Реализация репозитория модели Product на основе файлов."""

    PATH_DATA = os.path.join(app.BASE_DIR, 'data/product.json')

    def __init__(self, data=None):
        self.data: typing.List[models.Product] = data

        if data is None:
            self.__load_data()

    def __load_data(self) -> None:
        with open(self.PATH_DATA) as json_file:
            self.data = list(map(lambda item: models.Product(**item), json.load(json_file)))

    def get_all(self) -> typing.Iterable[models.Product]:
        return iter(self.data)

    def get_for_group(self, group_id: int) -> typing.Iterable[models.Product]:
        return filter(lambda item: item.group_id == group_id, self.data)

    def get(self, product_id: int) -> models.Product:
        products = list(filter(lambda item: item.id == product_id, self.data))
        if len(products) == 0:
            raise exceptions.ObjectDoesNotExist()
        if len(products) > 1:
            raise exceptions.MultipleObjectsReturned()

        return products[0]


class ApiProductRepository(repositories.ProductRepository):

    """Реализация репозитория модели Product на основе API."""

    @staticmethod
    def _serialize(
            data: typing.Union[typing.List[typing.Dict[str, str]], typing.Dict[str, str]],
            many: bool = False
    ) -> typing.Union[typing.Iterable[models.Product], models.Product]:
        if many:
            return map(lambda item: models.Product(**item), data)
        return models.Product(**data)

    def get_all(self) -> typing.Iterable[models.Product]:
        return self._serialize(api.get_all_products(), many=True)

    def get_for_group(self, group_id: int) -> typing.Iterable[models.Product]:
        return self._serialize(api.get_products_for_group(group_id), many=True)

    def get(self, product_id: int) -> typing.Iterable[models.Product]:
        return self._serialize(api.get_product(product_id))
