import typing
import os
import json

import app
from app.main.domain import models
from app.main.domain import repositories
from app.main import api, exceptions


class FileGroupRepository(repositories.GroupRepository):

    """Реализация репозитория модели Group на основе файлов."""

    PATH_DATA = os.path.join(app.BASE_DIR, 'data/group.json')

    def __init__(self, data=None):
        self.data: typing.List[models.Group] = data

        if data is None:
            self.__load_data()

    def __load_data(self) -> None:
        with open(self.PATH_DATA) as json_file:
            self.data = list(map(lambda item: models.Group(**item), json.load(json_file)))

    def get_all(self) -> typing.Iterable[models.Group]:
        return iter(self.data)

    def get_first_level(self) -> typing.Iterable[models.Group]:
        return filter(lambda item: item.parent_id is None and item.first, self.data)

    def get_additive_group(self) -> typing.Optional[models.Group]:
        try:
            return next(filter(lambda item: item.parent_id is None and item.additive, self.data))
        except StopIteration:
            return None

    def get_for_parent(self, parent_id: int):
        return filter(lambda item: item.parent_id == parent_id, self.data)

    def get(self, group_id: int) -> models.Group:
        products = list(filter(lambda item: item.id == group_id, self.data))
        if len(products) == 0:
            raise exceptions.ObjectDoesNotExist()
        if len(products) > 1:
            raise exceptions.MultipleObjectsReturned()

        return products[0]


class ApiGroupRepository(repositories.GroupRepository):

    """Реализация репозитория модели Group на основе API."""

    @staticmethod
    def _serialize(
            data: typing.Union[typing.List[typing.Dict[str, str]], typing.Dict[str, str]],
            many: bool = False
    ) -> typing.Union[typing.Iterable[models.Group], models.Group]:
        if many:
            return map(lambda item: models.Group(**item), data)
        return models.Group(**data)

    def get_all(self) -> typing.Iterable[models.Group]:
        return self._serialize(api.get_all_groups(), many=True)

    def get_first_level(self) -> typing.Iterable[models.Group]:
        return self._serialize(api.get_first_level_groups(), many=True)

    def get_additive_group(self) -> typing.Optional[models.Group]:
        # FIXME: группа добавок должна быть всегда одна, но с сервера возвращается список
        res = api.get_first_level_additive_groups()
        if len(res) > 0:
            return self._serialize(res[0])

    def get_for_parent(self, parent_id: int) -> typing.Iterable[models.Group]:
        return self._serialize(api.get_groups_for_parent(parent_id), many=True)

    def get(self, group_id: int) -> models.Group:
        return self._serialize(api.get_group(group_id), many=True)
