import typing

from app.core import config, requests

API: requests.Request = requests.Request(config.BASE_URL_SERVICE, headers={'X-API-Key': config.BOT_AUTH_KEY})


# -------------------------------------- API Group --------------------------------------

def get_group(group_id: int) -> typing.List[typing.Dict[str, str]]:
    return API.get(f'/api/v1/shops/groups/{group_id}').json()


def get_all_groups() -> typing.List[typing.Dict[str, str]]:
    return API.get('/api/v1/shops/groups').json()


def get_first_level_groups() -> typing.List[typing.Dict[str, str]]:
    return API.get('/api/v1/shops/groups', params={'first': True}).json()


def get_first_level_additive_groups() -> typing.List[typing.Dict[str, str]]:
    return API.get('/api/v1/shops/groups', params={'first': True, 'additive': True}).json()


def get_groups_for_parent(parent_id: int) -> typing.List[typing.Dict[str, str]]:
    return API.get(f'/api/v1/shops/groups', params={'parent_id': parent_id}).json()


# -------------------------------------- API Product --------------------------------------

def get_product(product_id: int) -> typing.Dict[str, str]:
    return API.get(f'/api/v1/shops/products/{product_id}').json()


def get_all_products() -> typing.List[typing.Dict[str, str]]:
    return API.get(f'/api/v1/shops/products').json()


def get_products_for_group(group_id: int) -> typing.List[typing.Dict[str, str]]:
    return API.get(f'/api/v1/shops/products', params={'group_id': group_id}).json()


# -------------------------------------- API Order --------------------------------------

def get_active_order(user_id: str) -> typing.Dict[str, str]:
    return API.get(f'/api/v1/orders/active/{user_id}').json()


def create_order(data: typing.Optional[typing.Dict[str, typing.Any]]) -> typing.Dict[str, str]:
    # FIXME: костыль, иначе не парсится, даже если использовать json.dump()
    data['id'] = data['id'].hex
    return API.post(f'/api/v1/orders', data=data).json()


def update_order(data: typing.Optional[typing.Dict[str, typing.Any]]) -> typing.Dict[str, str]:
    # FIXME: костыль, иначе не парсится, даже если использовать json.dump()
    data['id'] = data['id'].hex
    return API.put(f'/api/v1/orders', data=data).json()


def send_order(data: typing.Optional[typing.Dict[str, typing.Any]]) -> typing.Dict[str, str]:
    # FIXME: костыль, иначе не парсится, даже если использовать json.dump()
    data['id'] = data['id'].hex
    return API.post(f'/api/v1/orders/send-to-manager', data=data).json()


# -------------------------------------- API User --------------------------------------

def save_user(data: typing.Optional[typing.Dict[str, typing.Any]]) -> typing.Dict[str, str]:
    return API.post(f'/api/v1/users', data=data).json()
