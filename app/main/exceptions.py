class MultipleObjectsReturned(Exception):
    """Запрос возвратил несколько объектов, когда ожидался только один."""


class ObjectDoesNotExist(Exception):
    """Запрос НЕ возвратил объект, когда он ожидался."""
