import json
from uuid import UUID


class UUIDEncoder(json.JSONEncoder):
    def default(self, obj) -> str:
        if isinstance(obj, UUID):
            return obj.hex
        return json.JSONEncoder.default(self, obj)


def dump_data(data: dict) -> str:
    return json.dumps(data, cls=UUIDEncoder)


def load_data(data: str) -> dict:
    return json.loads(data)


def callback_data(event: str, **kwargs) -> str:
    data = {
        'event': event,
        **kwargs
    }

    return dump_data(data)
