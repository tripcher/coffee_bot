from functools import wraps

from app import storage


def dialog_history(func):
    @wraps(func)
    def wrapper(call, query: str, user_id: str, *args, **kwargs):
        last: str = storage.get_last_element_dialog_history(user_id)
        func(call, query=query, user_id=user_id, last=last, *args, **kwargs)
        storage.add_dialog_history(user_id, query)
    return wrapper
