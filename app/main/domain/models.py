import typing

import pydantic

from app.main.domain import events, const
from app.main import utils


class OrderStatus(object):

    """Статусы заказа."""

    STATUS_NOT_SELECTED = 'STATUS_NOT_SELECTED'  # Заказ пустой, товар не выбран
    STATUS_SELECTED = 'STATUS_SELECTED'  # Выбран хотя бы один товар
    STATUS_SENT = 'STATUS_SENT'  # Заказ отправлен менеджеру
    STATUS_ACCEPT = 'STATUS_ACCEPT'  # Заказ подтвержден менеджером
    STATUS_DENIED = 'STATUS_DENIED'  # Заказ отклонен менеджером
    STATUS_STOP_LIST = 'STATUS_STOP_LIST'  # Заказ отклонен менеджером, по причине отсутсвия товара
    STATUS_FAILURE = 'STATUS_FAILURE'  # Ошибка
    STATUS_READY = 'STATUS_READY'  # Заказ готов


class Group(pydantic.BaseModel):

    """Модель группы меню."""

    id: int
    text: str = ''
    parent_id: int = None
    # флаг для обосзначения групп, которые выводятся в главном меню
    first: bool = False
    # флаг для обозначения группы добавок, группа с True должна быть только одна
    additive: bool = False
    deleted: bool = False

    @property
    def callback_data(self) -> str:
        return self.get_callback_data(self.get_event())

    @staticmethod
    def get_event() -> str:
        return events.CALLBACK_GROUP

    def get_callback_data(self, event: str,  **kwargs):
        return utils.callback_data(event=event, id=self.id, **kwargs)

    def dict_view(self) -> dict:
        return {
            'text': self.text,
            'callback_data': self.callback_data
        }


class Product(pydantic.BaseModel):

    """Модель продукта."""

    id: int
    text: pydantic.constr(max_length=255)
    price: typing.Optional[float] = None
    volume: typing.Optional[int] = None
    dimension: typing.Optional[const.Dimension] = None
    deleted: bool = False
    group_id: int

    @property
    def short_text(self) -> str:
        text = self.text
        if self.volume is not None:
            text = f'{text} {self.volume}'
        if self.dimension is not None:
            text = f'{text} {self.dimension}'
        return text

    @property
    def full_text(self) -> str:
        text = self.text
        if self.volume is not None:
            text = f'{text} {self.volume}'
        if self.dimension is not None:
            text = f'{text} {self.dimension}'
        if self.price is not None:
            text = f'{text} - {self.price} ₽'

        return text

    @property
    def callback_data(self) -> str:
        return self.get_callback_data(self.get_event())

    @staticmethod
    def get_event() -> str:
        return events.CALLBACK_PRODUCT

    def get_callback_data(self, event: str, **kwargs):
        return utils.callback_data(event=event, id=self.id, **kwargs)

    def dict_view(self, text: str = None, data: dict = None) -> dict:
        return {
            'text': self.full_text if text is None else text,
            'callback_data': self.callback_data if data is None else self.get_callback_data(self.get_event(), **data)
        }
