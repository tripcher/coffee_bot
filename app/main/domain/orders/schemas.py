import typing

import pydantic
from app.main.domain import models as shop_models
from app.main.domain.orders import const


class OrderBase(pydantic.BaseModel):
    id: pydantic.UUID4
    sum: float
    status: const.OrderStatus
    products: typing.List['shop_models.Product']
    additives: typing.Dict[int, typing.List['shop_models.Product']]
    user_id: pydantic.constr(max_length=255)
