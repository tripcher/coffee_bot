import uuid
import typing
import pydantic

from app.main.domain import models as domain_models
from app.main.domain.orders import states, const


class Order(pydantic.BaseModel, object):
    """Модель заказа."""

    __state: typing.Optional['states.OrderState'] = None
    _states: typing.Tuple['states.OrderState.__class__'] = (
        states.NotSelectedState,
        states.SelectedState,
        states.SentState,
        states.AcceptState,
        states.DeniedState,
        states.StopListState,
        states.ReadyState,
        states.CloseState,
    )

    id: pydantic.UUID4 = uuid.uuid4()
    sum: float = 0
    status: const.OrderStatus = const.OrderStatus.STATUS_NOT_SELECTED
    products: typing.List['domain_models.Product'] = list()
    additives: typing.Dict[int, typing.List['domain_models.Product']] = dict()

    class Config:
        extra = pydantic.Extra.allow

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'shop', 'user', '_Order__state', '_state', '__state', *kwargs.get('exclude', ())}
        return super(Order, self).dict(*args, **kwargs)

    def _products_to_dict(
            self,
            products: typing.Optional[typing.List[domain_models.Product]]
    ) -> typing.Dict[int, dict]:
        products_dict = {}

        for product in products:
            quantity = products_dict.get(product.id, {'quantity': 0}).get('quantity') + 1
            products_dict[product.id] = {
                'text': product.short_text,
                'quantity': quantity,
                'sum': product.price * quantity,
                'additives':
                    self._products_to_dict(self.additives.get(product.id, [])) if self.additives is not None else {}
            }

        return products_dict

    def _text_view_product(self, product: typing.Dict[str, typing.Any], additive=False) -> str:
        tab = '    ' if additive else ''

        return '\n'.join([
            '{tab}{text} x {quantity} - {sum}'.format(tab=tab, **product),
            *[self._text_view_product(item, additive=True) for item in product.get('additives', {}).values()]
        ])

    @property
    def _state(self):
        # FIXME: подумать можно ли обойтись без поля status в моделе и как это будет мапиться в orm
        if self.__state is not None:
            return self.__state

        _states = [state for state in self._states if state.status == self.status]

        if len(_states):
            state_class = _states[0]
            state = state_class()
            state.context = self
            self.__state = state
            return state

        return None

    @property
    def current_text(self) -> str:
        """
        Возвращает текущий заказ в текстовом виде.
        Returns:
            str:
        """
        if self.products:
            products_dict = self._products_to_dict(self.products)
            status_text = []

            if self._state.status not in [const.OrderStatus.STATUS_NOT_SELECTED, const.OrderStatus.STATUS_SELECTED]:
                status_text = ['Статус вашего заказа: "{status}"'.format(status=self.status)]

            return '\n'.join(
                status_text + [
                    'Ваш заказ:',
                    *[self._text_view_product(product) for product in products_dict.values()],
                    '\nИтого: {sum}\n'.format(sum=sum([product.get('sum') for product in products_dict.values()])),
                ]
            )
        return ''

    def set_state(self, state: 'states.OrderState') -> None:
        """
        Изменяет состояние (статус) заказа.
        Args:
            state (states.OrderState): Состояние.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self.status = state.status
        state.context = self
        self.__state = state

    def add_product(self, product: 'domain_models.Product') -> None:
        """
        Добовить продукт к текущему заказу.
        Args:
            product (domain_models.Product): Продукт.

        Returns:
            None:

        Raises:
            OrderFail:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.add_product(product)

    def delete_product(self, product: 'domain_models.Product') -> None:
        """
        Удалить продукт из текущего заказа.
        Args:
            product (domain_models.Product): Продукт.

        Returns:
            None:

        Raises:
            OrderFail:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.delete_product(product)

    def send_to_managers(self, user_id: str) -> None:
        """
        Отправить заказ менеджеру магазина.
        Args:
            user_id (str): Идентификатор пользователя.
        Returns:
            None:

        Raises:
            OrderFail:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.send_to_managers(user_id)

    def accept_from_manager(self) -> None:
        """
        Подтвердить заказ.
        Args:

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.accept_from_manager()

    def denied_from_manager(self) -> None:
        """
        Отклонить заказ.
        Args:

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.denied_from_manager()

    def stop_list_from_manager(self) -> None:
        """
        Отклонить заказ, по причине отсутсвия товара.
        Args:

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.stop_list_from_manager()

    def fail(self, error: str = None) -> None:
        """
        Перевести заказ в статус ошибки.
        Args:
            error (str): Комментарий.

        Returns:
            None:

        """
        self._state.fail(error)

    def ready(self, comment: str = None) -> None:
        """
        Перевести заказ в статус готово.
        Args:
            comment (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.ready(comment)

    def close(self, comment: str = None) -> None:
        """
        Закрыть заказ.
        Args:
            comment (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.close(comment)
