import abc
import typing

from app.main import api
from app.main.domain import models as domain_models
from . import models, const, exceptions, schemas, decorators


class OrderState(abc.ABC):
    """Состояние заказа."""

    status: const.OrderStatus = None

    _context: 'models.Order' = None

    @property
    def context(self) -> typing.Optional['models.Order']:
        return self._context

    @context.setter
    def context(self, context: 'models.Order') -> None:
        self._context = context

    @abc.abstractmethod
    def add_product(self, product: 'domain_models.Product') -> None:
        pass

    @abc.abstractmethod
    def delete_product(self, product: 'domain_models.Product') -> None:
        pass

    @abc.abstractmethod
    def send_to_managers(self, user_id: str) -> None:
        pass

    @abc.abstractmethod
    def accept_from_manager(self) -> None:
        """
        Подтвердить заказ.
        Args:

        Returns:
            None:

        Raises:
            OrderFail:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        pass

    @abc.abstractmethod
    def denied_from_manager(self) -> None:
        pass

    @abc.abstractmethod
    def stop_list_from_manager(self) -> None:
        pass

    @abc.abstractmethod
    def fail(self, error: str = None, raises_exception: bool = False) -> None:
        pass

    @abc.abstractmethod
    def ready(self, comment: str = None) -> None:
        pass

    @abc.abstractmethod
    def close(self, comment: str = None) -> None:
        pass


class BaseOrderState(OrderState):

    def _raise_exception(self, method: str):
        raise exceptions.OrderStateMethodException(method, self.status)

    def add_product(self, product: 'domain_models.Product') -> None:
        self._raise_exception('add_product')

    def delete_product(self, product: 'domain_models.Product') -> None:
        self._raise_exception('delete_product')

    def send_to_managers(self, user_id: str) -> None:
        self._raise_exception('sent_to_manager')

    def accept_from_manager(self) -> None:
        self._raise_exception('accept_from_manager')

    def denied_from_manager(self) -> None:
        self._raise_exception('denied_from_manager')

    def stop_list_from_manager(self) -> None:
        self._raise_exception('stop_list_from_manager')

    def fail(self, error: str = None, raises_exception: bool = False) -> None:
        self.context.set_state(FailureState())
        if raises_exception:
            raise exceptions.OrderFail(error)

    def ready(self, comment: str = None) -> None:
        self._raise_exception('ready')

    def close(self, comment: str = None) -> None:
        self._raise_exception('close')


class NotSelectedState(BaseOrderState):

    status = const.OrderStatus.STATUS_NOT_SELECTED

    def add_product(self, product: 'domain_models.Product') -> None:
        if product.deleted:
            # TODO: raise exception
            pass
        self.context.products.append(product)
        self.context.set_state(SelectedState())

    def delete_product(self, product: 'domain_models.Product') -> None:
        pass


class SelectedState(BaseOrderState):

    status = const.OrderStatus.STATUS_SELECTED

    @decorators.fail_state_method_error
    def add_product(self, product: 'domain_models.Product') -> None:
        if product.deleted:
            # TODO: raise exception
            pass
        self.context.products.append(product)

    @decorators.fail_state_method_error
    def delete_product(self, product: 'domain_models.Product') -> None:
        self.context.products.remove(product)

        if len(self.context.products) == 0:
            self.context.set_state(NotSelectedState())

    @decorators.fail_http_error
    @decorators.fail_state_method_error
    def send_to_managers(self, user_id: str) -> None:
        data = schemas.OrderBase(**self.context.dict(exclude={'user_id'}), user_id=user_id).dict()
        order_data = api.send_order(data=data)

        if order_data.get('status') == const.OrderStatus.STATUS_SENT:
            self.context.set_state(SentState())
        else:
            self.fail(error='Server returned status not equal STATUS_SENT', raises_exception=True)


class SentState(BaseOrderState):

    status = const.OrderStatus.STATUS_SENT

    @decorators.fail_state_method_error
    def accept_from_manager(self) -> None:
        self.context.set_state(AcceptState())

    @decorators.fail_state_method_error
    def denied_from_manager(self) -> None:
        self.context.set_state(DeniedState())

    @decorators.fail_state_method_error
    def stop_list_from_manager(self) -> None:
        self.context.set_state(StopListState())


class AcceptState(BaseOrderState):

    status = const.OrderStatus.STATUS_ACCEPT


class DeniedState(BaseOrderState):

    status = const.OrderStatus.STATUS_DENIED


class StopListState(BaseOrderState):

    status = const.OrderStatus.STATUS_STOP_LIST


class FailureState(BaseOrderState):

    status = const.OrderStatus.STATUS_FAILURE


class ReadyState(BaseOrderState):

    status = const.OrderStatus.STATUS_READY


class CloseState(BaseOrderState):

    status = const.OrderStatus.STATUS_CLOSED
