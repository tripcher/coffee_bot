from functools import wraps

from requests import exceptions as requests_exceptions
from . import exceptions


def fail_state_method_error(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            func(self, *args, **kwargs)
        except exceptions.OrderStateMethodException as exc:
            self.fail(error=str(exc), raises_exception=True)
    return wrapper


def fail_http_error(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            func(self, *args, **kwargs)
        except requests_exceptions.HTTPError as exc:
            self.fail(error=str(exc), raises_exception=True)
    return wrapper
