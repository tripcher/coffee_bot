import abc
import typing

from app.main.domain import models


class GroupRepository(abc.ABC):

    """Интерфейс репозитория модели Group."""

    @abc.abstractmethod
    def get_all(self) -> typing.Iterable[models.Group]:
        """
        Возвращает все группы магазина.

        Returns:
            typing.Iterable[models.Group]: Список групп.
        """
        pass

    @abc.abstractmethod
    def get_first_level(self) -> typing.Iterable[models.Group]:
        """
        Возвращает группы для первого уровня меню.

        Должны выполняться условия:
        first = True
        parent = None
        additive = False

        Returns:
            typing.Iterable[models.Group]: Список групп.

        """
        pass

    @abc.abstractmethod
    def get_additive_group(self) -> typing.Optional[models.Group]:
        """
        Возвращает группу для первого уровня меню добавок.

        Должны выполняться условия:
        first = True
        parent = None
        additive = True

        Returns:
            typing.Optional[models.Group]:  Группа добавок.
        """
        pass

    @abc.abstractmethod
    def get_for_parent(self, parent_id: int) -> typing.Iterable[models.Group]:
        """
        Возвращает все подгруппы группы.
        Args:
            parent_id (int): Идентификатор группы.

        Returns:
            typing.Iterable[models.Group]: Список групп.
        """
        pass

    @abc.abstractmethod
    def get(self, group_id: int) -> models.Group:
        """
        Возвращает группу.
        Args:
            group_id (int):

        Returns:
            models.Group:
        """
        pass


class ProductRepository(abc.ABC):

    """Интерфейс репозитория модели Product."""

    @abc.abstractmethod
    def get_all(self) -> typing.Iterable[models.Product]:
        pass

    @abc.abstractmethod
    def get_for_group(self, group_id: int) -> typing.Iterable[models.Product]:
        pass

    @abc.abstractmethod
    def get(self, product_id: int) -> models.Product:
        pass
