import typing

import pydantic


class UserBase(pydantic.BaseModel):
    platform_id: pydantic.constr(max_length=255)
    platform_login: typing.Optional[pydantic.constr(max_length=255)]
    platform_chat_id: pydantic.constr(max_length=255)


class UserMessage(pydantic.BaseModel):
    platform_id: pydantic.constr(max_length=255)
    platform_chat_id: pydantic.constr(max_length=255)
    comment: typing.Optional[pydantic.constr(strip_whitespace=True)]
