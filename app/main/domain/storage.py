import abc
import typing

from app.main.domain import models
from app.main.domain.orders import models as orders_models


class Storage(abc.ABC):

    @abc.abstractmethod
    def new_dialog_history(self, user_id: str) -> None:
        """
        Создает или обнуляет историю диалога для пользователя.
        Args:
            user_id (str): Идентификатор пользователя.

        Returns:
            None:

        """
        pass

    @abc.abstractmethod
    def get_dialog_history(self, user_id: str) -> typing.List[str]:
        """
        Возвращает историю диалога для пользователя.
        Args:
            user_id (str): Идентификатор пользователя.

        Returns:
            typing.List[str]: Список действий пользователя в меню.

        """
        pass

    @abc.abstractmethod
    def add_dialog_history(self, user_id: str, element: str) -> None:
        """
        Добавляет действие в историю диалога для пользователя.
        Args:
            user_id (str):  Идентификатор пользователя.
            element (str):  действие.

        Returns: None

        """
        pass

    @abc.abstractmethod
    def get_last_element_dialog_history(self, user_id: str) -> str:
        """
        Возвращает последнее действие из истории диалога для пользователя.
        Args:
            user_id (str):  Идентификатор пользователя.

        Returns:
              str: Действие.

        """
        pass

    @abc.abstractmethod
    def pop_last_element_dialog_history(self, user_id: str) -> str:
        """
        Возвращает и удаляет из списка последнее действие из истории диалога для пользователя.
        Args:
            user_id (str):  Идентификатор пользователя.

        Returns:
             str:  Действие.

        """
        pass

    @abc.abstractmethod
    def add_order(self, user_id: str) -> orders_models.Order:
        """
        Создает новый заказ, если у текущего пользователя нет заказов.
        Args:
            user_id (str):  Идентификатор пользователя.

        Returns:
            orders_models.Order: Текущий заказ пользователя.

        """
        pass

    @abc.abstractmethod
    def edit_order(self, user_id: str, order: orders_models.Order) -> orders_models.Order:
        """
        Обновляет заказ в текущем хранилище.
        Args:
            user_id (str):  Идентификатор пользователя.
            order (orders_models.Order):

        Returns:
            orders_models.Order: Текущий заказ пользователя.

        """
        pass

    @abc.abstractmethod
    def get_order(self, user_id: str) -> orders_models.Order:
        """
        Возвращает текущий заказ пользователя.
        Args:
            user_id (str):  Идентификатор пользователя.

        Returns:
            orders_models.Order: Текущий заказ пользователя.

        Raises:
            KeyError:
        """
        pass

    @abc.abstractmethod
    def add_product_for_order(self, user_id: str, product: models.Product) -> None:
        """
        Добавляет продукт в текущий заказ пользователя.
        Args:
            user_id (str):  Идентификатор пользователя.
            product (models.Product): - продукт.

        Returns:
            None:

        """
        pass

    @abc.abstractmethod
    def add_additive_product_for_order(self, user_id: str, additive: models.Product, product_id: int) -> None:
        """
        Добавляет добавку к продукту в текущем заказе пользователя.
        Args:
            user_id (str):  Идентификатор пользователя.
            additive (models.Product): Добавка.
            product_id (int):  Идетификатор продукта.

        Returns:
            None:

        """
        pass

    @abc.abstractmethod
    def del_product_for_order(self, user_id: str, product: models.Product, full=False) -> None:
        """
        Удаляет продукт или все продукты из текущего заказа пользователя.
        Args:
            user_id (str):  Идентификатор пользователя.
            product (models.Product): Продукт.
            full (bool): Удалить всю позицию.

        Returns:
            None:

        """
        pass

    @abc.abstractmethod
    def del_additive_product_for_order(
            self,
            user_id: str,
            additive: models.Product,
            product_id: int,
            full=False
    ) -> None:
        """
        Удаляет продукт или все продукты из текущего заказа пользователя.
        Args:
            user_id (str):  Идентификатор пользователя.
            additive (models.Product): Добавка.
            product_id (int): Идентификатор продукта.
            full (bool): Удалить всю позицию.
        Returns:
            None:

        """
        pass

    @abc.abstractmethod
    def get_last_product_id_for_order(self, user_id: str) -> typing.Optional[int]:
        """
        Возвращает id последнего выбранного продукта в текущем заказе, если ничего не найдено - вернет None.
        Args:
            user_id (str):  Идентификатор пользователя.

        Returns:
            typing.Optional[int]: id последнего выбранного продукта.

        """

    @abc.abstractmethod
    def get_last_product_id_for_dialog_history(self, user_id: str) -> typing.Optional[int]:
        """
        Возвращает id последнего выбранного продукта в меню, если ничего не найдено - вернет None.
        Args:
            user_id (str):  Идентификатор пользователя.

        Returns:
            typing.Optional[int]: id последнего выбранного продукта.

        """
