from app import storage
from app.main.domain.orders import models as orders_models, exceptions


def get_current_text(user_id: str) -> str:
    """
    Возвращает заказ пользователя в текстовом виде.
    Args:
        user_id:

    Returns:
        str:
    """
    order: orders_models.Order = storage.get_order(user_id)
    return order.current_text


def send(user_id: str) -> bool:
    """
    Отправляет текущий заказ пользователя менеджеру.
    Args:
        user_id:

    Returns:
        bool:

    Raises:
        OrderStateMethodException:
    """
    result = True
    order = storage.get_order(user_id)
    # Отправляем текущий заказ
    try:
        order.send_to_managers(user_id)
    except exceptions.OrderFail:
        result = False

    storage.edit_order(user_id, order)

    return result


def accept_from_manager(user_id: str) -> orders_models.Order:
    """
    Менеджер подтверждает заказ.
    Args:
        user_id:

    Returns:
        bool:

    Raises:
        OrderStateMethodException:
        KeyError:
    """
    order = storage.get_order(user_id)
    # Подтверждаем текущий заказ
    try:
        order.accept_from_manager()
    except exceptions.OrderFail as exc:
        pass
    order = storage.edit_order(user_id, order)
    return order
