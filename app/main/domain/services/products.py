import typing

from app import storage
from app.main.domain import models, repositories
from app.main.domain.orders import models as orders_models


def get_for_group(
        group_id: int,
        product_rep: repositories.ProductRepository
) -> typing.Iterable[models.Product]:
    """
    Возвращает все продукты для группы.
    Args:
        group_id (int):
        product_rep (repositories.ProductRepository):

    Returns:
        typing.Iterable[models.Product]:
    """
    return product_rep.get_for_group(group_id)


def get_for_additive_group(
        group_rep: repositories.GroupRepository,
        product_rep: repositories.ProductRepository
) -> typing.Iterable[models.Product]:
    """
    Возвращает все продукты для группы добавок.
    Args:
        group_rep (repositories.GroupRepository):
        product_rep (repositories.ProductRepository):

    Returns:
        typing.Iterable[models.Product]:
    """
    additive_group: typing.Optional[models.Group] = group_rep.get_additive_group()
    if additive_group:
        return product_rep.get_for_group(additive_group.id)

    return []


def _select_product(user_id: str, product: models.Product, count: int = None) -> None:
    if count is None:
        if product in storage.get_order(user_id).products:
            return
        storage.add_product_for_order(user_id, product)
        return

    for i in range(count):
        storage.add_product_for_order(user_id, product)


def _select_additive(user_id: str, additive: models.Product, parent_id: int, count: int = None) -> None:

    if count is None:
        if additive in storage.get_order(user_id).additives.get(parent_id, []):
            return
        storage.add_additive_product_for_order(user_id, additive, parent_id)
        return

    for i in range(count):
        storage.add_additive_product_for_order(user_id, additive, parent_id)


def select(
        user_id: str,
        product_id: int,
        product_rep: repositories.ProductRepository,
        parent_id: int = None,
        count: int = None,
) -> models.Product:
    product: models.Product = product_rep.get(product_id)

    if parent_id is not None:
        _select_additive(user_id, product, parent_id, count=count)
    else:
        _select_product(user_id, product, count=count)

    return product


def remove(
        user_id: str,
        product_id: int,
        product_rep: repositories.ProductRepository,
        parent_id: int = None,
        full=False
) -> models.Product:
    product: models.Product = product_rep.get(product_id)

    if parent_id is not None:
        # нужно проверять, что не добавка
        storage.del_additive_product_for_order(user_id, product, parent_id, full=full)
    else:
        storage.del_product_for_order(user_id, product, full=full)

    return product


def count(user_id: str, product_id: int, product_rep: repositories.ProductRepository) -> int:
    order: orders_models.Order = storage.get_order(user_id)
    product: models.Product = product_rep.get(product_id)
    return order.products.count(product)
