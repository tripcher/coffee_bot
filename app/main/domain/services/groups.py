import typing

from app.main.domain import models, repositories


def get_first_level(group_rep: repositories.GroupRepository) -> typing.Iterable[models.Group]:
    """
    Возвращает группы для первого уровня меню.
    Args:
        group_rep (repositories.GroupRepository):

    Returns:
        typing.Iterable[models.Group]:
    """
    return group_rep.get_first_level()


def get_for_parent(parent_id: int, group_rep: repositories.GroupRepository) -> typing.Iterable[models.Group]:
    """
    Возвращает все подгруппы группы.
    Args:
        parent_id (int):
        group_rep (repositories.GroupRepository):

    Returns:
        typing.Iterable[models.Group]:
    """
    return group_rep.get_for_parent(parent_id)


def get_for_additive_group(group_rep: repositories.GroupRepository) -> typing.Iterable[models.Group]:
    """
    Возвращает все подгруппы главной группы добавок (группы добавок).
    Args:
        group_rep (repositories.GroupRepository):

    Returns:
        typing.Iterable[models.Group]
    """
    additive_group: typing.Optional[models.Group] = group_rep.get_additive_group()
    if additive_group:
        return group_rep.get_for_parent(additive_group.id)

    return []
