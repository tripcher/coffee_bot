import typing

from app import storage
from app.main import api
from app.main.domain import schemas, services, repositories, models
from app.main.domain.orders import models as orders_models, const as orders_const


def start(user: schemas.UserBase) -> orders_models.Order:
    """
    Начинает новый диалог, сохраняет пользователя.
    Args:
        user (schemas.UserBase):

    Returns:
        orders_models.Order: Текущий заказ пользователя.
    """
    # Создаем или обнуляем историю диалога
    storage.new_dialog_history(user.platform_id)
    # Создаем новый заказ
    order = storage.add_order(user.platform_id)

    # Сохраняем пользователя на сервере
    data = user

    api.save_user(data=data.dict())

    return order


def pop_last_element_dialog_history(user_id: str) -> str:
    """
    Возвращает и удаляет из списка последнее действие из истории диалога для пользователя.
    Args:
        user_id (str):  Идентификатор пользователя.

    Returns:
         str:  Действие.

    """
    return storage.pop_last_element_dialog_history(user_id)


def start_facade(
        user: schemas.UserBase,
        group_rep: repositories.GroupRepository,
) -> typing.Tuple[orders_models.Order, typing.List[models.Group]]:
    """
    Начинает новый диалог, сохраняет пользователя.

    Возвращает группы для первого уровня меню.

    Args:
        user (schemas.UserBase):
        group_rep (repositories.GroupRepository):

    Returns:
        orders_models.Order: Текущий заказ пользователя.
        typing.Iterable[models.Group]:

    """
    order = start(user)
    groups = []
    if order.status in [orders_const.OrderStatus.STATUS_NOT_SELECTED, orders_const.OrderStatus.STATUS_SELECTED]:
        groups = services.groups.get_first_level(group_rep)
    return order, groups
