from app import bot

from app.contrib import dependency
from app.contrib.telegram.menu import views
from app.contrib.telegram.core import callback_func, decorators
from app.main import utils
from app.main.domain import events, services

__all__ = ('group_select_callback_query_handler', 'product_select_callback_query_handler',)


@decorators.callback_handler(
    callback_func=callback_func(events.CALLBACK_GROUP),
    clear=True,
    history=True
)
def group_select_callback_query_handler(call, id, *args, **kwargs) -> None:

    group_id = id

    groups = map(lambda item: item.dict_view(), services.groups.get_for_parent(
        parent_id=group_id,
        group_rep=dependency.get_group_repository()
    ))

    products = map(lambda item: item.dict_view(), services.products.get_for_group(
        group_id=group_id,
        product_rep=dependency.get_product_repository()
    ))

    bot.send_message(call.message.chat.id, **views.group_message_view(call, groups=groups, products=products,
                                                                      *args, **kwargs))


@decorators.callback_handler(
    callback_func=callback_func(events.CALLBACK_GROUP_ADDITIVE),
    clear=True,
    history=True
)
def group_additive_select_callback_query_handler(call, id,  *args, **kwargs) -> None:
    groups = map(
        lambda item: item.dict_view(),
        services.groups.get_for_additive_group(group_rep=dependency.get_group_repository())
    )

    products = map(
        lambda item: item.dict_view(data={'parent_id': id}),
        services.products.get_for_additive_group(
            group_rep=dependency.get_group_repository(),
            product_rep=dependency.get_product_repository()
        )
    )

    bot.send_message(call.message.chat.id, **views.group_message_view(call, groups=groups, products=products,
                                                                      *args, **kwargs))


@decorators.callback_handler(callback_func=callback_func(events.CALLBACK_PRODUCT_ADD), clear=True)
def product_select_add_callback_query_handler(call, id, count, parent_id: int = None, *args, **kwargs) -> None:
    product = services.products.select(
        kwargs.get('user_id'),
        id,
        parent_id=parent_id,
        count=count,
        product_rep=dependency.get_product_repository()
    )

    bot.send_message(
        call.message.chat.id,
        **views.product_message_view(
            call,
            product_count=services.products.count(
                kwargs.get('user_id'),
                id,
                product_rep=dependency.get_product_repository()
            ),
            product=product,
            parent_id=parent_id,
            *args,
            **kwargs
        )
    )


@decorators.callback_handler(callback_func=callback_func(events.CALLBACK_PRODUCT_REMOVE), clear=True)
def product_remove_callback_query_handler(call, id, parent_id: int = None, *args, **kwargs) -> None:
    product = services.products.remove(
        kwargs.get('user_id'),
        id, parent_id=parent_id,
        product_rep=dependency.get_product_repository()
    )

    bot.send_message(
        call.message.chat.id,
        **views.product_message_view(
            call,
            product_count=services.products.count(
                kwargs.get('user_id'),
                id,
                product_rep=dependency.get_product_repository()
            ),
            product=product,
            parent_id=parent_id,
            *args,
            **kwargs
        )
    )


@decorators.callback_handler(callback_func=callback_func(events.CALLBACK_PRODUCT_FULL_REMOVE))
def product_full_remove_callback_query_handler(call, id, *args, **kwargs) -> None:
    services.products.remove(
        kwargs.get('user_id'),
        id,
        parent_id=kwargs.get('parent_id'),
        full=True,
        product_rep=dependency.get_product_repository()
    )

    call.data = utils.callback_data(events.CALLBACK_BACK)
    bot.process_new_callback_query([call])


@decorators.callback_handler(
    callback_func=callback_func(events.CALLBACK_PRODUCT),
    clear=True,
    history=True
)
def product_select_callback_query_handler(call, id: int, parent_id: int = None, *args, **kwargs) -> None:

    product = services.products.select(
        kwargs.get('user_id'),
        id,
        parent_id=parent_id,
        product_rep=dependency.get_product_repository()
    )

    bot.send_message(
        call.message.chat.id,
        **views.product_message_view(
            call,
            product_count=services.products.count(
                kwargs.get('user_id'),
                id,
                product_rep=dependency.get_product_repository()
            ),
            product=product,
            parent_id=parent_id,
            *args,
            **kwargs
        )
    )


@decorators.callback_handler(
    callback_func=callback_func(events.CALLBACK_MAKE_ORDER),
    clear=True,
    history=True
)
def make_order(call, *args, **kwargs) -> None:
    bot.send_message(
        call.message.chat.id,
        **views.make_order(
            call,
            *args,
            **kwargs
        )
    )


@decorators.callback_handler(
    callback_func=callback_func(events.CALLBACK_MAKE_ORDER_YES),
    clear=True,
    history=True
)
def make_order_yes(call, *args, **kwargs) -> None:
    bot.send_message(
        call.message.chat.id,
        **views.make_order_yes(
            call,
            *args,
            **kwargs
        )
    )


@decorators.callback_handler(
    callback_func=callback_func(events.CALLBACK_SEND_ORDER),
    clear=True,
    history=True
)
def send_order(call, user_id: str, *args, **kwargs) -> None:
    """

    Args:
        call:
        user_id (str):
        *args:
        **kwargs:

    Returns:
        None:

    Raises:
        OrderStateMethodException:
    """

    sent = services.orders.send(user_id)

    bot.send_message(
        call.message.chat.id,
        **views.send_order(
            call,
            sent=sent,
            *args,
            **kwargs
        )
    )
