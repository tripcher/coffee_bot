import os
import typing

from telebot import types

from app.main.domain import models, events
from app.main import utils
from app.contrib.telegram.core import decorators

THIS_DIR = os.path.dirname(os.path.realpath(__file__))


@decorators.callback_view(back=True, order=True)
def group_message_view(
        call: types.CallbackQuery,
        *args,
        **kwargs
) -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:

    text = 'Выберете один из элементов меню'

    groups: typing.Iterable[dict] = kwargs.get('groups', [])
    products: typing.Iterable[dict] = kwargs.get('products', [])

    reply_markup = types.InlineKeyboardMarkup()
    reply_markup.row_width = 1
    reply_markup.add(*[types.InlineKeyboardButton(**item_menu) for item_menu in groups])
    reply_markup.add(*[types.InlineKeyboardButton(**item_menu) for item_menu in products])

    return {
        'text': text,
        'reply_markup': reply_markup
    }


@decorators.callback_view(back=True, order=True)
def product_message_view(
        call: types.CallbackQuery,
        *args,
        **kwargs
) -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:
    text: str = 'Выберете один из элементов меню'

    product: models.Product = kwargs.get('product')
    product_count: int = kwargs.get('product_count', 1)
    parent_id: typing.Optional[int] = kwargs.get('parent_id')

    reply_markup = types.InlineKeyboardMarkup()
    reply_markup.row_width = 2
    reply_markup.add(
        types.InlineKeyboardButton(
            text='Перейти к оформлению заказа',
            callback_data=utils.callback_data(events.CALLBACK_MAKE_ORDER)
        )
    )
    if parent_id is None:
        reply_markup.add(
            types.InlineKeyboardButton(
                text='Выбрать добавки',
                callback_data=product.get_callback_data(event=events.CALLBACK_GROUP_ADDITIVE)
            )
        )

    reply_markup.row(
        types.InlineKeyboardButton(
            text='+ 1',
            callback_data=product.get_callback_data(
                event=events.CALLBACK_PRODUCT_ADD,
                count=1,
                parent_id=parent_id
            )
        ),
        types.InlineKeyboardButton(
            text='+ 2',
            callback_data=product.get_callback_data(
                event=events.CALLBACK_PRODUCT_ADD,
                count=2,
                parent_id=parent_id
            )
        ),
    )

    if product_count == 1:
        reply_markup.add(
            types.InlineKeyboardButton(
                text='❌ Удалить позицию',
                callback_data=product.get_callback_data(
                    event=events.CALLBACK_PRODUCT_FULL_REMOVE,
                    parent_id=parent_id
                )
            )
        ),
    else:
        reply_markup.row(
            types.InlineKeyboardButton(
                text='- 1',
                callback_data=product.get_callback_data(
                    event=events.CALLBACK_PRODUCT_REMOVE,
                    parent_id=parent_id
                )
            ),
            types.InlineKeyboardButton(
                text='❌ Удалить',
                callback_data=product.get_callback_data(
                    event=events.CALLBACK_PRODUCT_FULL_REMOVE,
                    parent_id=parent_id
                )
            )
        )

    return {
        'text': text,
        'reply_markup': reply_markup
    }


@decorators.callback_view(order=True)
def make_order(
        call: types.CallbackQuery,
        *args,
        **kwargs
) -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:

    text = 'Все верно?'

    reply_markup = types.InlineKeyboardMarkup()
    reply_markup.row_width = 2
    reply_markup.row(
        types.InlineKeyboardButton(
            text='Да',
            callback_data=utils.callback_data(events.CALLBACK_MAKE_ORDER_YES)
        ),
        types.InlineKeyboardButton(
            text='Нет',
            callback_data=utils.callback_data(events.CALLBACK_BACK)
        ),
    )

    return {
        'text': text,
        'reply_markup': reply_markup
    }


@decorators.callback_view(back=True, order=True)
def make_order_yes(
        call: types.CallbackQuery,
        *args,
        **kwargs
) -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:
    text = 'Выберите способ оплаты:'

    reply_markup = types.InlineKeyboardMarkup()
    reply_markup.row_width = 1
    reply_markup.add(
        types.InlineKeyboardButton(
            text='Оплатить картой',
            callback_data=utils.callback_data(events.CALLBACK_PAY_CARD)
        )
    )
    reply_markup.add(
        types.InlineKeyboardButton(
            text='Оплатить в заведении',
            callback_data=utils.callback_data(events.CALLBACK_SEND_ORDER)
        ),
    )

    return {
        'text': text,
        'reply_markup': reply_markup
    }


@decorators.callback_view()
def send_order(
        call: types.CallbackQuery,
        *args,
        **kwargs
) -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:

    sent: bool = kwargs.get('sent', False)

    if sent:
        text = 'Ваш заказ принят в обработку.\n' \
               'Статус вашего заказа будет обновляться в чате.'
    else:
        text = 'Что-то пошло не так, попробуйте сделать заказ позже'

    reply_markup = types.InlineKeyboardMarkup()

    return {
        'text': text,
        'reply_markup': reply_markup
    }
