from app import bot

from app.contrib.telegram.webhooks import views


def accept_from_manager(chat_id: str):
    bot.send_message(chat_id, **views.accept_from_manager())
