import typing
from telebot import types


def accept_from_manager() -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:

    data = [
        {
            'text': 'О сервисе',
            'callback_data': 'about'
        }
    ]

    reply_markup = types.InlineKeyboardMarkup()
    reply_markup.row_width = 1
    reply_markup.add(*[types.InlineKeyboardButton(**item_menu) for item_menu in data])

    return {
        'text': 'Менеджер подтвердил заказ',
        'reply_markup': reply_markup
    }
