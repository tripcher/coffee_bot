import json

from telebot import types

from app.main import utils


def callback_func(event: str):

    def _callback_func(call: types.CallbackQuery) -> bool:
        data = utils.load_data(call.data)
        return data.get('event') == event

    return _callback_func
