from telebot import types


class BackInlineKeyboardMarkup(types.InlineKeyboardButton):
    def __init__(self, back_callback: str, **kwargs):
        super(BackInlineKeyboardMarkup, self).__init__(text='⬅️', callback_data=back_callback, **kwargs)
