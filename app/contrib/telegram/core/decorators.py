from functools import wraps

from telebot import types

from app import bot
from app.main.domain import events, services
from app.main.decorators import dialog_history
from app.main import utils
from app.contrib.telegram.core import markup


def callback_view(back=False, order=False):
    def decorator(func):
        @wraps(func)
        def wrapper(call: types.CallbackQuery, *args, **kwargs):
            view = func(call, *args, **kwargs)
            if back:
                back_markup = markup.BackInlineKeyboardMarkup(
                    back_callback=utils.callback_data(event=events.CALLBACK_BACK)
                )
                view.get('reply_markup').add(back_markup)
            if order:
                order_text = services.orders.get_current_text(kwargs.get('user_id'))
                view['text'] = '\n\n'.join([view.get('text', ''), order_text])
            return view

        return wrapper
    return decorator


def clear_message(func):
    @wraps(func)
    def wrapper(call, *args, **kwargs):
        bot.delete_message(call.message.chat.id, call.message.message_id)
        func(call, *args, **kwargs)
    return wrapper


# TODO: добавить handler_error и отлавливать OrderStateMethodException
def callback_handler(callback_func, clear: bool = False, history: bool = False, **kwargs):

    def decorator(func):
        @wraps(func)
        @bot.callback_query_handler(func=callback_func, **kwargs)
        def wrapper(call, *args, **kwargs):
            handler = func

            if clear:
                handler = clear_message(handler)

            if history:
                handler = dialog_history(handler)

            data = utils.load_data(call.data)

            handler(call, query=call.data, user_id=str(call.from_user.id), **data)

        return wrapper
    return decorator
