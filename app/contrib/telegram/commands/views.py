import typing
from telebot import types

from app.main.domain import models
from app.contrib.telegram.core import decorators


def _start_view(
        groups: typing.List[models.Group],
        text: typing.Optional[str] = None
) -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:
    text = text or 'Что желаете заказать?'

    data = [
        *map(lambda item: item.dict_view(), groups),
        {
            'text': 'О сервисе',
            'callback_data': 'about'
        }
    ]

    reply_markup = types.InlineKeyboardMarkup()
    reply_markup.row_width = 1
    reply_markup.add(*[types.InlineKeyboardButton(**item_menu) for item_menu in data])

    return {
        'text': text,
        'reply_markup': reply_markup
    }


def start_message_view(
        current: types.Message,
        groups: typing.List[models.Group],
        text: typing.Optional[str] = None
) -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:
    return _start_view(groups, text=text)


@decorators.callback_view()
def start_view(
        call: types.CallbackQuery,
        groups: typing.List[models.Group],
        text: typing.Optional[str] = None,
        *args,
        **kwargs
) -> typing.Dict[str, typing.Union[str, types.InlineKeyboardMarkup]]:
    return _start_view(groups, text=text)
