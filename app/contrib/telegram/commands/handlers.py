from app import bot
from app.main.domain import events, services, schemas
from app.contrib import dependency
from app.contrib.telegram.commands import views
from app.contrib.telegram.core import callback_func, decorators

__all__ = ('start_message', )


@bot.message_handler(commands=['start'])
def start_message(message):
    user = schemas.UserBase(
        platform_id=message.from_user.id,
        platform_login=message.from_user.username,
        platform_chat_id=message.chat.id
    )

    order, groups = services.core.start_facade(
        user=user,
        group_rep=dependency.get_group_repository()
    )
    bot.send_message(message.chat.id, **views.start_message_view(message, groups, text=order.current_text))


@decorators.callback_handler(callback_func=callback_func(events.CALLBACK_START), clear=True, history=True)
def start_callback_query_handler(call, *args, **kwargs):
    user = schemas.UserBase(
        platform_id=call.from_user.id,
        platform_login=call.from_user.username,
        platform_chat_id=call.message.chat.id
    )

    order, groups = services.core.start_facade(
        user=user,
        group_rep=dependency.get_group_repository(),
    )
    bot.send_message(call.message.chat.id, **views.start_view(call, groups, text=order.current_text, *args, **kwargs))


@decorators.callback_handler(callback_func=callback_func(events.CALLBACK_BACK))
def back_callback_query_handler(call, *args, **kwargs):
    # вытаскивает текущий элемент меню из истории
    services.core.pop_last_element_dialog_history(str(call.from_user.id))

    # вытаскивает последний элемент меню из истории (тот что был перед текущим), и возвращается на этот элемент меню
    call.data = services.core.pop_last_element_dialog_history(str(call.from_user.id))
    bot.process_new_callback_query([call])
