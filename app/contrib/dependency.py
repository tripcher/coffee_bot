from app.main.domain import repositories as domain_rep
from app.main.implement import repositories


def get_group_repository() -> domain_rep.GroupRepository:
    return repositories.group.ApiGroupRepository()


def get_product_repository() -> domain_rep.ProductRepository:
    return repositories.product.ApiProductRepository()
