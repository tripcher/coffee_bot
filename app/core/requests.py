import typing
import requests
import urllib.parse


def raise_for_status(func: typing.Callable):
    def wrapper(*args, **kwargs):
        # TODO: доделать обработку ошибок
        response: requests.Response = func(*args, **kwargs)
        print(response)
        print(response.json())
        response.raise_for_status()
        return response
    return wrapper


class Request(object):

    def __init__(self, base_url: str, headers: typing.Dict[str, str] = None):
        self.base_url = base_url
        self.headers = headers

    @raise_for_status
    def get(
            self,
            url: str,
            params: typing.Dict[str, typing.Union[str, bool, int]] = None,
            **kwargs
    ) -> requests.Response:
        kwargs = {
            'headers': self.headers,
            **kwargs
        }
        return requests.get(urllib.parse.urljoin(self.base_url, url), params=params, **kwargs)

    @raise_for_status
    def post(self, url: str, data: typing.Dict[str, typing.Any], **kwargs):
        kwargs = {
            'headers': self.headers,
            **kwargs
        }
        return requests.post(urllib.parse.urljoin(self.base_url, url), json=data, **kwargs)

    @raise_for_status
    def put(self, url: str, data: typing.Dict[str, typing.Any], **kwargs):
        kwargs = {
            'headers': self.headers,
            **kwargs
        }
        return requests.put(urllib.parse.urljoin(self.base_url, url), json=data, **kwargs)
