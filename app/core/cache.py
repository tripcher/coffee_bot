import json

from app.main import utils
from pymemcache.client import base

from app.core import config

__all__ = ('get_memcache',)


class JsonSerde(object):
    def serialize(self, key, value):
        if isinstance(value, str):
            return value.encode('utf-8'), 1
        return utils.dump_data(value).encode('utf-8'), 2

    def deserialize(self, key, value, flags):
       if flags == 1:
           return value.decode('utf-8')
       if flags == 2:
           return utils.load_data(value.decode('utf-8'))
       raise Exception("Unknown serialization format")


def get_memcache():
    print('CREATE CLIENT MEMCACHE')
    client = base.Client((config.MEMCACHED_HOST, config.MEMCACHED_PORT), serde=JsonSerde())
    return client

