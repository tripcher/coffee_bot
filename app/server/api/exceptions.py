import enum
from fastapi import exceptions, status


class KeysError(str, enum.Enum):
    """Ключи ошибок."""

    NOT_VALID_OPERATION = 'NOT_VALID_OPERATION'
    NOT_FOUND = 'NOT_FOUND'


class NotFoundHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=KeysError.NOT_FOUND)


class NotValidOperationHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail=KeysError.NOT_VALID_OPERATION)
