from fastapi import APIRouter

from app.main.domain import schemas, services
from app.main.domain.orders import models as orders_models, exceptions as orders_exceptions
from app.server.api import exceptions
from app.contrib.telegram.webhooks import handlers
router = APIRouter()


@router.post('/orders/accept-from-manager', response_model=orders_models.Order)
async def accept_from_manager(data: schemas.UserMessage):
    try:
        order = services.orders.accept_from_manager(data.platform_id)
    except orders_exceptions.OrderStateMethodException:
        raise exceptions.NotValidOperationHTTPException()
    else:
        handlers.accept_from_manager(data.platform_chat_id)
        return order
