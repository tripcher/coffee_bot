from fastapi import APIRouter

from .core import router as core_router


__all__ = ('router_v1',)

router_v1 = APIRouter()

router_v1.include_router(
    core_router,
    tags=["core"]
)
