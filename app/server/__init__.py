import os

from fastapi import FastAPI

from app.server import api

APP_DIR = os.path.dirname(os.path.realpath(__file__))

MAIN_DIR = os.path.dirname(APP_DIR)


app = FastAPI()

app.include_router(
    api.router_v1,
    prefix="/api/v1"
)


@app.get("/")
async def root():
    return {"message": "Hello World"}
