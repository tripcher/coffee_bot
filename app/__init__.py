import os

import telebot

from app.main.domain import storage as domain_storage
from app.main.implement import storage as impl_storage

APP_DIR = os.path.dirname(os.path.realpath(__file__))

BASE_DIR = os.path.dirname(APP_DIR)


# FIXME: получать токен с сервера или вынести в настройки
def get_bot() -> telebot.TeleBot:
    return telebot.TeleBot('936207928:AAHO1piODSSwf7mfishN-9v3O5pzEmJgksY')


def get_storage() -> domain_storage.Storage:
    print('CREATE STORAGE')
    return impl_storage.MemcachedStorage()


bot: telebot.TeleBot = get_bot()

storage: domain_storage.Storage = get_storage()
